# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sso', '0004_useraccesslog_app_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraccesslog',
            name='is_company_user',
            field=models.BooleanField(default=True),
        ),
    ]
