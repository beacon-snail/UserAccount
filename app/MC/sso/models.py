from django.db import models
from django.utils import timezone
from MC.settings import APP_ID


# Create your models here.
class UserAccessLog(models.Model):
    user = models.CharField(max_length=200)
    access_url = models.CharField(max_length=200)
    access_ip = models.CharField(max_length=20)
    company_id = models.CharField(max_length=200, blank=True, null=True)
    access_time = models.DateTimeField(default=timezone.now)
    app_id = models.CharField(max_length=50, default=APP_ID)
    is_company_user = models.BooleanField(default=True)

    class Meta:
        ordering = ("-access_time", )
        db_table = "access_log"

    def __str__(self):
        return self.user
