from django.db import models


# Create your models here.
# class TbUser(models.Model):
#     username = models.CharField(unique=True, max_length=100)
#     password = models.CharField(max_length=100)
#     nickname = models.CharField(max_length=100)
#     reg_time = models.DateTimeField(blank=True, null=True,auto_now_add=True)
#     updateddt = models.DateTimeField(blank=True, null=True,auto_now=True)
#     avatar = models.CharField(max_length=100,null=True)
#     token = models.CharField(max_length=100, blank=True, null=True)
#     expire_time = models.DateTimeField(blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'tb_user'
#
#
# class TbUserExtended(models.Model):
#     uid = models.IntegerField()
#     phone = models.CharField(max_length=20)
#     email = models.CharField(max_length=100)
#     ip = models.CharField(max_length=16, blank=True, null=True)
#     reg_time = models.DateTimeField(blank=True, null=True,auto_now_add=True)
#     updateddt = models.DateTimeField(blank=True, null=True,auto_now=True)
#     status = models.CharField(max_length=2)
#     expiration_date = models.DateTimeField()
#     updatedby = models.CharField(max_length=100, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'tb_user_extended'
#
#
# class TbUserLogs(models.Model):
#     uid = models.IntegerField()
#     platform = models.CharField(max_length=255, blank=True, null=True)
#     logintime = models.DateTimeField(blank=True, null=True,auto_now=True)
#     ip = models.CharField(max_length=16, blank=True, null=True)
#     status = models.CharField(max_length=255, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'tb_user_logs'
#
#
# class TbUserThird(models.Model):
#     uid = models.IntegerField()
#     platform = models.CharField(max_length=255, blank=True, null=True)
#     uid_extra = models.CharField(max_length=255, blank=True, null=True)
#     other = models.CharField(max_length=255, blank=True, null=True)
#     reg_time = models.DateTimeField(blank=True, null=True,auto_now_add=True)
#
#     class Meta:
#         managed = False
#         db_table = 'tb_user_third'
class TCaProcess(models.Model):
    f_ca_process_id = models.AutoField(primary_key=True)
    f_ca_type = models.IntegerField(blank=True, null=True)
    f_ca_id = models.IntegerField(blank=True, null=True)
    f_process_type = models.IntegerField(blank=True, null=True)
    f_operation_account = models.ForeignKey('TUserInfo', db_column='f_operation_account', blank=True, null=True)
    f_operation_time = models.BigIntegerField(blank=True, null=True)
    f_remark = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_ca_process'


class TClientCipherKey(models.Model):
    f_c_cipher_key_id = models.AutoField(primary_key=True)
    f_c_rsa_public_key = models.TextField(blank=True, null=True)
    f_c_name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_client_cipher_key'


class TEnterpriseCertification(models.Model):
    f_enterprise_certification_id = models.AutoField(primary_key=True)
    f_account = models.ForeignKey('TUserInfo', db_column='f_account')
    f_enterprise_user_role = models.IntegerField(blank=True, null=True)
    f_enterprise_name = models.CharField(max_length=100, blank=True, null=True)
    f_social_credit_code = models.CharField(max_length=50, blank=True, null=True)
    f_organization_code = models.CharField(max_length=50, blank=True, null=True)
    f_business_mode = models.CharField(max_length=50, blank=True, null=True)
    f_business_scope = models.TextField(blank=True, null=True)
    f_enterprise_scale = models.CharField(max_length=50, blank=True, null=True)
    f_registered_address = models.CharField(max_length=255, blank=True, null=True)
    f_business_license_scan = models.TextField(blank=True, null=True)
    f_authorised_letter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_enterprise_certification'


class TEnterpriseData(models.Model):
    tenant_id = models.IntegerField()
    db_name = models.CharField(max_length=100)
    database_type = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 't_enterprise_data'


class TIdpsBypass(models.Model):
    f_idps_bypass_id = models.AutoField(primary_key=True)
    f_account = models.CharField(max_length=20, blank=True, null=True)
    f_role_type = models.IntegerField(blank=True, null=True)
    f_use_type = models.IntegerField(blank=True, null=True)
    f_group_name = models.CharField(max_length=50, blank=True, null=True)
    f_group_id = models.IntegerField(blank=True, null=True)
    f_bypass_number = models.IntegerField(blank=True, null=True)
    f_bypass_valid_time = models.BigIntegerField(blank=True, null=True)
    f_ownership = models.IntegerField(blank=True, null=True)
    f_authorized_enterprise = models.IntegerField(blank=True, null=True)
    f_limit_use_user = models.IntegerField(blank=True, null=True)
    f_create_time = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_idps_bypass'


class TIdpsRecord(models.Model):
    f_idps_record_id = models.AutoField(primary_key=True)
    f_account = models.CharField(max_length=20, blank=True, null=True)
    f_role_type = models.IntegerField(blank=True, null=True)
    f_use_type = models.IntegerField(blank=True, null=True)
    f_group_name = models.CharField(max_length=50, blank=True, null=True)
    f_group_id = models.IntegerField(blank=True, null=True)
    f_record_number = models.IntegerField(blank=True, null=True)
    f_record_valid_time = models.BigIntegerField(blank=True, null=True)
    f_record_save_days = models.IntegerField(blank=True, null=True)
    f_ownership = models.IntegerField(blank=True, null=True)
    f_authorized_enterprise = models.IntegerField(blank=True, null=True)
    f_limit_use_user = models.IntegerField(blank=True, null=True)
    f_create_time = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_idps_record'


class TMicroService(models.Model):
    f_micro_service_type = models.CharField(primary_key=True, max_length=100)
    f_micro_service_table = models.CharField(max_length=100, blank=True, null=True)
    f_micro_service_license = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_micro_service'


class TMiddleware(models.Model):
    f_middleware_id = models.AutoField(primary_key=True)
    f_account = models.ForeignKey('TUserInfo', db_column='f_account', blank=True, null=True)
    f_micro_service_type = models.ForeignKey(TMicroService, db_column='f_micro_service_type', blank=True, null=True)
    f_micro_service_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_middleware'


class TPersonalAuthentication(models.Model):
    f_personal_authentication_id = models.AutoField(primary_key=True)
    f_account = models.ForeignKey('TUserInfo', db_column='f_account')
    f_id_type = models.IntegerField(blank=True, null=True)
    f_id_number = models.CharField(max_length=50, blank=True, null=True)
    f_id_frontal_picture = models.TextField(blank=True, null=True)
    f_id_reverse_picture = models.TextField(blank=True, null=True)
    f_id_handheld_picture = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_personal_authentication'


class TQuestionnaire(models.Model):
    f_questionnaire_id = models.AutoField(primary_key=True)
    f_account = models.ForeignKey('TUserInfo', db_column='f_account', blank=True, null=True)
    f_visitor_phone = models.ForeignKey('TVisitorInfo', db_column='f_visitor_phone', blank=True, null=True)
    f_subject_json = models.TextField(blank=True, null=True)
    f_answer_json = models.TextField(blank=True, null=True)
    f_answer_time = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_questionnaire'


class TSecuritySetting(models.Model):
    f_security_setting_id = models.AutoField(primary_key=True)
    f_account = models.ForeignKey('TUserInfo', db_column='f_account', blank=True, null=True)
    f_passwd_complex_ratio = models.IntegerField(blank=True, null=True)
    f_phone_bind = models.IntegerField(blank=True, null=True)
    f_email_bind = models.IntegerField(blank=True, null=True)
    f_operation_verify = models.IntegerField(blank=True, null=True)
    f_question_one = models.CharField(max_length=255, blank=True, null=True)
    f_answer_one = models.CharField(max_length=255, blank=True, null=True)
    f_question_two = models.CharField(max_length=255, blank=True, null=True)
    f_answer_two = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_security_setting'


class TServerCipherKey(models.Model):
    f_s_cipher_key_id = models.AutoField(primary_key=True)
    f_s_rsa_public_key = models.TextField()
    f_s_rsa_private_key = models.TextField()

    class Meta:
        managed = False
        db_table = 't_server_cipher_key'


class TUserInfo(models.Model):
    f_user_info_id = models.AutoField(unique=True,primary_key=True)
    f_account = models.CharField(primary_key=True, max_length=20)
    f_passwd = models.CharField(max_length=100)
    f_nickname = models.CharField(max_length=50)
    f_picture = models.TextField(blank=True, null=True)
    f_user_name = models.CharField(max_length=50, blank=True, null=True)
    f_mobile_phone = models.CharField(max_length=50, blank=True, null=True)
    f_email = models.CharField(max_length=100, blank=True, null=True)
    f_consent_statement = models.IntegerField(blank=True, null=True)
    f_registration_time = models.BigIntegerField(blank=True, null=True)
    f_company_name = models.CharField(max_length=100, blank=True, null=True)
    f_job_titles = models.CharField(max_length=50, blank=True, null=True)
    f_contact_address = models.CharField(max_length=255, blank=True, null=True)
    f_company_phone = models.CharField(max_length=50, blank=True, null=True)
    f_company_fax = models.CharField(max_length=50, blank=True, null=True)
    f_personal_authentication_state = models.IntegerField(blank=True, null=True)
    f_enterprise_certification_state = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_user_info'


class TVisitorInfo(models.Model):
    f_visitor_info_id = models.AutoField(unique=True,primary_key=True)
    f_visitor_name = models.CharField(max_length=50, blank=True, null=True)
    f_visitor_phone = models.CharField(primary_key=True, max_length=50)
    f_visitor_enterprise = models.CharField(max_length=100, blank=True, null=True)
    f_visitor_sex = models.CharField(max_length=20, blank=True, null=True)
    f_registration_time = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't_visitor_info'
