# from rest_framework import serializers
# from userAccount import models
from userAccount.functions.tools import *
#
#
# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = models.TbUser
#         # fields = ("username","password")
#         fields = "__all__"
#
#
# class LoginSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = models.TbUser
#         fields = ("username","password")
#
#
# class LogoutSerializer(serializers.Serializer):
#     HTTP_X_CSRFTOKEN = serializers.CharField(required=True,max_length=100)
#
#
# class ExtendUserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = models.TbUserExtended
#         fields = "__all__"
#
#
# class MinUserSerializer(serializers.Serializer):
#     password = serializers.CharField(required=True, allow_blank=True, max_length=100)
#     nickname = serializers.CharField(required=True, allow_blank=True, max_length=100)
#     avatar = serializers.ImageField(required=True)
#     mobile = serializers.CharField(required=True, allow_blank=True, max_length=11)
#     email = serializers.EmailField(required=True)
#
#     def is_valid(self, raise_exception=False):
#         result = super(MinUserSerializer, self).is_valid()
#         password_check = False
#         account_check = False
#         email_check = False
#         assert verify_password(self.data["password"])
#         assert check_account(self.data["username"])
#         assert check_email(self.data["email"])
#         return result
#
#
# class MaxUserSerialiazer(serializers.Serializer):
#     password = serializers.CharField(required=True, allow_blank=True, max_length=100)
#     nickname = serializers.CharField(required=True, allow_blank=True, max_length=100)
#     avatar = serializers.ImageField(required=True)
#     phone = serializers.CharField(required=True, allow_blank=True, max_length=11)
#     email = serializers.EmailField(required=True)
#     ip = serializers.IPAddressField()
#     uid = serializers.IntegerField(required=True)
#     update_time = serializers.DateTimeField()
from rest_framework import serializers
from userAccount import models


class UserSerializer(serializers.Serializer):
        nickname = serializers.CharField(required=True, allow_blank=True, max_length=100)
        phone = serializers.CharField(max_length=11,required=True)
        password = serializers.CharField(required=True,max_length=100)
        code = serializers.CharField(required=True)

        def is_valid(self, raise_exception=False):
            result = super(UserSerializer, self).is_valid()
            phone_check = False
            password_check = False
            account_check = False
            # email_check = False
            assert check_mobile(self.data["phone"])
            assert verify_password(self.data["password"])
            assert check_account(self.data["nickname"])
            # assert check_email(self.data["email"])
            return result
        # model = models.TUserInfo
        # fields = ("f_nickname","f_mobile_phone","password")
        # fields = "__all__"


class UserDetailSerializer(serializers.ModelSerializer):
    # f_nickname = serializers.CharField(required=True, allow_blank=True, max_length=100,label='用户昵称')
    # f_mobile_phone = serializers.CharField(max_length=11, required=True,label="用户手机号")
    # f_passwd = serializers.CharField(required=True, max_length=100,label="用户密码")

    class Meta:
            model = models.TUserInfo
            fields = "__all__"


class LoginSerializer(serializers.Serializer):

    # model = models.TUserInfo
    # fields = ("f_mobile_phone","password")
    # fields = "__all__"
    phone = serializers.CharField(max_length=11, required=True)
    password = serializers.CharField(required=True, max_length=100)


class CompanyDataSerializer(serializers.ModelSerializer):

    class Meta:
            model = models.TEnterpriseData
            fields = "__all__"


class CheckSerializer(serializers.Serializer):
    account = serializers.CharField(max_length=20, required=True)

    def is_valid(self, raise_exception=False):
        result = super(CheckSerializer, self).is_valid()
        phone_check = False
        assert check_mobile(self.data["account"])
        return result


class BatchAccountSerializer(serializers.Serializer):
    num = serializers.CharField(required=True)
    nickname = serializers.CharField(required=True)
    password = serializers.CharField(required=True)