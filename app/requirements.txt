django==1.8.7
djangorestframework==3.6.3
django-rest-swagger==2.2.0
django-cors-headers==2.1.0
setuptools==12.0.5
pymysql==0.7.11