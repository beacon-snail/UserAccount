# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UserAccessLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('user', models.CharField(max_length=30)),
                ('access_url', models.CharField(max_length=200)),
                ('access_ip', models.CharField(max_length=20)),
                ('company_id', models.CharField(max_length=200)),
                ('access_time', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'db_table': 'access_log',
                'ordering': ('-access_time',),
            },
        ),
    ]
