# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sso', '0002_auto_20180622_1414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='useraccesslog',
            name='company_id',
            field=models.CharField(max_length=200, blank=True, null=True),
        ),
    ]
