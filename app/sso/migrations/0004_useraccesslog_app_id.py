# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sso', '0003_auto_20180622_1416'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraccesslog',
            name='app_id',
            field=models.CharField(max_length=50, default='beacon:123456'),
        ),
    ]
